import bpy
from . LUZLoad import Load

from bpy.types import Panel, Operator
from bpy.props import StringProperty
class LoadWorld(Operator):
    bl_idname = 'lu.load_world'
    bl_label = 'Load a LEGO Universe World'
    filepath = StringProperty(subtype="FILE_PATH")
    filter_glob = StringProperty(
            default="*.luz",
            options={'HIDDEN'},
            )
    def execute(self, context):
        file = open(self.filepath, 'rb')
        Load.load_luz(file)
        self.report({'INFO'}, 'World Loaded!')
        return {'FINISHED'}
    
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
    
#panel class
class WorldLoadPanel(Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_label = 'Operations'
    bl_context = 'objectmode'
    bl_category = 'LU World'
    
    # adding ui
    def draw(self, context):
        layout = self.layout
        layout.operator("lu.load_world", text = 'Load a World')
        layout.prop(context.scene, 'CDClient')
        
def register():
    bpy.utils.register_class(LoadWorld)
    bpy.utils.register_class(WorldLoadPanel)
    initSceneProperties()

def unregister():
    bpy.utils.unregister_class(WorldLoadPanel)
    bpy.utils.unregister_class(WorldLoad)

def initSceneProperties():
    bpy.types.Scene.CDClient = StringProperty(name="CDClient Path", subtype="FILE_PATH")