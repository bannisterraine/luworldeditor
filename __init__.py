bl_info = {
    "name":         "LU World Editor",
    "description":  "LEGO Universe world editing add-on",
    "author":       "Kendal Nelson",
    "version":      (0,0,1),
    "blender":      (2,77,0),
    "location":     "Blender",
    "category":     "Import-Export",
    "warning":      ""
}

import bpy
from .dWorldEditor import register
from .dWorldEditor import unregister

def register():
    dWorldEditor.register()
def unregister():
    dWorldEditor.unregister()
