import bpy
import sys
import os
import sqlite3

from . bitstream import BitStream, c_float, c_int, c_int64, c_ubyte, c_uint, c_uint64, c_ushort

class Load:
    def lvl_parse_chunk_type_2001(stream, db):
        for _ in range(stream.read(c_uint)):
            object_id = stream.read(c_int64) # seems like the object id, but without some bits
            lot = stream.read(c_uint)
            unknown1 = stream.read(c_uint)
            unknown2 = stream.read(c_uint)
            scale = 5
            x = stream.read(c_float)/scale
            y = stream.read(c_float)/scale
            z = stream.read(c_float)/scale
            rw = stream.read(c_float)
            rx = stream.read(c_float)
            ry = stream.read(c_float)
            rz = stream.read(c_float)
            scale = stream.read(c_float)
            config_data = stream.read(str, length_type=c_uint)
            config_data = config_data.replace("{", "<crlbrktopen>").replace("}", "<crlbrktclose>").replace("\\", "<backslash>") # for some reason these characters aren't properly escaped when sent to Tk
            assert stream.read(c_uint) == 0
            if lot == 176:
                bpy.ops.mesh.primitive_cube_add(location=(x,z,y),)
                object = bpy.context.active_object
                object.rotation_mode='QUATERNION'
                object.rotation_quaternion = [rw, rx, rz, ry]
                lot = config_data[config_data.index("spawntemplate")+16:config_data.index("\n", config_data.index("spawntemplate")+16)]
                object.name = lot
                object['lot'] = lot
                object['config'] = config_data
                try:
                    object.name = db.execute("select name from Objects where id == "+object['lot']).fetchone()[0]
                except:
                    print("Error with DB")
    def parse_lvl(stream, db):
            if stream[0:4] == b"CHNK":
                # newer lvl file structure
                # chunk based
                while not stream.all_read():
                    assert stream._read_offset//8 % 16 == 0 # seems everything is aligned like this?
                    start_pos = stream._read_offset//8
                    assert stream.read(bytes, length=4) == b"CHNK"
                    chunk_type = stream.read(c_uint)
                    assert stream.read(c_ushort) == 1
                    assert stream.read(c_ushort) in (1, 2)
                    chunk_length = stream.read(c_uint)
                    data_pos = stream.read(c_uint)
                    stream._read_offset = data_pos * 8
                    assert stream._read_offset//8 % 16 == 0
                    if chunk_type == 1000:
                        pass
                    elif chunk_type == 2000:
                        pass
                    elif chunk_type == 2001:
                        Load.lvl_parse_chunk_type_2001(stream, db)
                    elif chunk_type == 2002:
                        pass
                    stream._read_offset = (start_pos + chunk_length) * 8 # go to the next CHNK
            else:
                # older lvl file structure
                stream.skip_read(265)
                stream.read(str, char_size=1, length_type=c_uint)
                for _ in range(5):
                    stream.read(str, char_size=1, length_type=c_uint)
                stream.skip_read(4)
                for _ in range(stream.read(c_uint)):
                    stream.read(c_float), stream.read(c_float), stream.read(c_float)
                Load.lvl_parse_chunk_type_2001(stream, scene, db)
                
    def load_luz(file):
        try:
            db = sqlite3.connect(bpy.context.scene['CDClient'])
        except:
            print("Can't connect to the database")
            return
        stream = BitStream(file.read())
        version = stream.read(c_uint)
        unknown1 = stream.read(c_uint)
        world_id = stream.read(c_uint)
        if version >= 38:
            spawnpoint_pos = stream.read(c_float), stream.read(c_float), stream.read(c_float)
            spawnpoint_rot = stream.read(c_float), stream.read(c_float), stream.read(c_float), stream.read(c_float)
            #zone = self.tree.insert("", END, text="Zone", values=(version, unknown1, world_id, spawnpoint_pos, spawnpoint_rot))
        
        if version >= 37:
            number_of_scenes = stream.read(c_uint)
        else:
            number_of_scenes = stream.read(c_ubyte)

        for _ in range(number_of_scenes):
            filename = stream.read(str, char_size=1, length_type=c_ubyte)
            scene_id = stream.read(c_uint64)
            scene_name = stream.read(str, char_size=1, length_type=c_ubyte)
            #scene = self.tree.insert(scenes, END, text="Scene", values=(filename, scene_id, scene_name))
            assert stream.read(bytes, length=3)
            lvl_path = os.path.join(os.path.dirname(file.name), filename)
            if os.path.exists(lvl_path):
                with open(lvl_path, "rb") as lvl:
                    print("Loading lvl", filename)
                    try:
                        Load.parse_lvl(BitStream(lvl.read()), db)
                    except Exception:
                        import traceback
                        traceback.print_exc()
                    print("Loaded!")
        assert stream.read(c_ubyte) == 0